import pytest
import os
import unittest.mock
from unittest.mock import Mock, patch
import paho.mqtt.publish as publish
from lib.mqttPublisher import Mqtt_publisher
#mocker.patch('application.is_windows', return_value=True)

class TestMqttPublisher:
# ======================================================================================================================
    @patch('paho.mqtt.publish.single')
    def test_01_notification_OK(self, mocked_simple):
        mocked_simple.return_value = 0
        # test that
        test_notification = {
            "channel": "test-chanNel",
            "msg": {
                "agent": "Agent Fronius",
                "status": "OK",
                "data": "Starting up"
            }
        }
        topics = {}
        topics["notification"] = "mqtt_notification_topic"
        topics["timeseries"] = "mqtt_ts_topic_base"
        topics["state"] = "mqtt_topic_state"
        mqtt_host = "192.168.100.26"
        mqtt_port = 1883
        subject = "test"

        mqttPub = Mqtt_publisher(mqtt_host, mqtt_port, subject, topics)
        res = mqttPub.send_notification(test_notification)
        assert res == 0

    # ----------------------------------------------------------------------------------------------------------------------
    @patch('paho.mqtt.publish.single')
    def test_02_notification_topic_NOK(self, mocked_simple):
        # the notification topic is not set, the returned value shall be -1
        mocked_simple.return_value = 0
        test_notification = {
            "channel": "test-chanNel",
            "msg": {
                "agent": "Agent Fronius",
                "status": "OK",
                "data": "Starting up"
            }
        }
        topics = {}
        topics["timeseries"] = "mqtt_ts_topic_base"
        topics["state"] = "mqtt_topic_state"
        mqtt_host = "192.168.100.26"
        mqtt_port = 1883
        subject = "test"

        mqttPub = Mqtt_publisher(mqtt_host, mqtt_port, subject, topics)
        res = mqttPub.send_notification(test_notification)
        assert res==-1

    # ----------------------------------------------------------------------------------------------------------------------
    @patch('paho.mqtt.publish.single')
    def test_03_notification_mesg_malformed(self, mocked_simple):
        # the notification topic is not set, the returned value shall be -1
        mocked_simple.return_value = 0
        test_notification = {
            "chanxxxnelXXX": "test-chanNel",
            "msg": {
                "agent": "Agent Fronius",
                "status": "OK",
                "data": "Starting up"
            }
        }
        topics = {}
        topics["timeseries"] = "mqtt_ts_topic_base"
        topics["state"] = "mqtt_topic_state"
        mqtt_host = "192.168.100.26"
        mqtt_port = 1883
        subject = "test"

        mqttPub = Mqtt_publisher(mqtt_host, mqtt_port, subject, topics)
        res = mqttPub.send_notification(test_notification)
        assert res==-1

    # ----------------------------------------------------------------------------------------------------------------------
    def test_02(self):
        # test that
        test_notification = {
                                "channel": "test-chanNel",
                                "msg": {
                                    "agent": "Agent Fronius",
                                    "status": "OK",
                                    "data": "Starting up"
                                }
                            }
        topics = {}
        topics["notification"] = "mqtt_notification_topic"
        topics["timeseries"] = "mqtt_ts_topic_base"
        topics["state"] = "mqtt_topic_state"
        mqtt_host = "192.168.100.26"
        mqtt_port = 1883
        subject = "test"
        mqttPub = Mqtt_publisher(mqtt_host, mqtt_port, subject, topics)
        mqttPub.send_notification(test_notification)