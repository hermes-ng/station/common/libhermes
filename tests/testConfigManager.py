import pytest
import os

from ..src.libhermes.configManager import ConfigManager

class TestConfigManager:
# ======================================================================================================================
    def test_01_loaded_config(self):
        # test that loaded_config is compliant when trying to load a non given param with a default value
        config_params=[
            {"name": "PARAM_WITH_DEFAULT", "default": "INFO"}
        ]
        expected_loaded_config = [{'default': 'INFO', 'name': 'PARAM_WITH_DEFAULT', 'value': 'INFO'}]
        cm = ConfigManager(config_params)
        assert cm.loaded_config == expected_loaded_config
# ----------------------------------------------------------------------------------------------------------------------
    def test_01_status(self):
        # test that load_params returns True when trying to load a non given param with a default value
        config_params=[
            {"name": "PARAM_WITH_DEFAULT", "default": "INFO"}
        ]
        cm = ConfigManager(config_params)
        assert cm.status == True
# ----------------------------------------------------------------------------------------------------------------------
    def test_01_value_default(self):
        # test that load_params returns the default value when trying to load a non given param with a default value
        config_params=[
            {"name": "PARAM_WITH_DEFAULT", "default": "INFO"}
        ]
        cm = ConfigManager(config_params)
        assert cm.get_param("PARAM_WITH_DEFAULT") == "INFO"
# ----------------------------------------------------------------------------------------------------------------------
    def test_01_value_given(self):
        # test that load_params returns the default value when trying to load a given param with a default value
        config_params=[
            {"name": "PARAM_WITH_DEFAULT", "default": "INFO"}
        ]
        os.environ["PARAM_WITH_DEFAULT"] = "DEBUG"
        cm = ConfigManager(config_params)
        assert cm.get_param("PARAM_WITH_DEFAULT") == "DEBUG"
# ======================================================================================================================
    def test_02_loaded_config_value_not_given(self):
        # test that loaded_config is compliant when trying to load a non given param with a default value
        config_params = [
            {"name": "PARAM_WITHOUT_DEFAULT"}
        ]
        expected_loaded_config = [{'name': 'PARAM_WITHOUT_DEFAULT', 'value': None}]
        cm = ConfigManager(config_params)
        assert cm.loaded_config == expected_loaded_config
# ----------------------------------------------------------------------------------------------------------------------
    def test_02_loaded_config_value_given(self):
        # test that loaded_config is compliant when trying to load a non given param with a default value
        config_params=[
            {"name": "PARAM_WITHOUT_DEFAULT"}
        ]
        os.environ["PARAM_WITHOUT_DEFAULT"] = "DEBUG"
        expected_loaded_config = [{'name': 'PARAM_WITHOUT_DEFAULT', 'value': "DEBUG"}]
        cm = ConfigManager(config_params)
        assert cm.loaded_config == expected_loaded_config
# ----------------------------------------------------------------------------------------------------------------------
    def test_02_status(self):
            # test that load_params returns False when trying to load a non given param with no default value
            config_params=[
                {"name": "PARAM_WITHOUT_DEFAULT2"}
            ]
            cm = ConfigManager(config_params)
            assert cm.status == False
# ----------------------------------------------------------------------------------------------------------------------
    def test_02_value_given(self):
        # test that load_params returns the value from environ when trying to load a param with a default value
        config_params=[
            {"name": "PARAM_WITHOUT_DEFAULT"}
        ]
        os.environ["PARAM_WITHOUT_DEFAULT"] = "DEBUG"
        cm = ConfigManager(config_params)
        assert cm.get_param("PARAM_WITHOUT_DEFAULT") == "DEBUG"
# ----------------------------------------------------------------------------------------------------------------------
    def test_02_value_not_given(self):
        # test that load_params returns the value from environ when trying to load a param with a default value
        config_params=[
            {"name": "PARAM_WITHOUT_DEFAULT3"}
        ]
        cm = ConfigManager(config_params)
        assert cm.get_param("PARAM_WITHOUT_DEFAULT3") == None
# ======================================================================================================================
    def test_03_loaded_config_1(self):
        # test that loaded_config is compliant when trying to load a non given param with a default value
        config_params=[
            {"name": "PARAM_WITH_DEFAULT1", "default": "INFO"},
            {"name": "PARAM_WITHOUT_DEFAULT4"}
        ]
        expected_loaded_config = [{'default': 'INFO', 'name': 'PARAM_WITH_DEFAULT1', 'value': 'INFO'},  {"name": "PARAM_WITHOUT_DEFAULT4", "value": None}]
        cm = ConfigManager(config_params)
        assert cm.loaded_config == expected_loaded_config
        assert cm.status == False
# ----------------------------------------------------------------------------------------------------------------------
    def test_03_loaded_config_2(self):
        # test that loaded_config is compliant when trying to load a non given param with a default value
        config_params=[
            {"name": "PARAM_WITH_DEFAULT1", "default": "INFO"},
            {"name": "PARAM_WITHOUT_DEFAULT4"}
        ]
        os.environ["PARAM_WITH_DEFAULT1"] = "DEBUG"
        expected_loaded_config = [{'default': 'INFO', 'name': 'PARAM_WITH_DEFAULT1', 'value': 'DEBUG'},  {"name": "PARAM_WITHOUT_DEFAULT4", "value": None}]
        cm = ConfigManager(config_params)
        assert cm.loaded_config == expected_loaded_config
# ----------------------------------------------------------------------------------------------------------------------
    def test_03_loaded_config_3(self):
        # test that loaded_config is compliant when trying to load a non given param with a default value
        config_params=[
            {"name": "PARAM_WITH_DEFAULT1", "default": "INFO"},
            {"name": "PARAM_WITHOUT_DEFAULT4"}
        ]
        os.environ["PARAM_WITH_DEFAULT1"] = "DEBUG"
        os.environ["PARAM_WITHOUT_DEFAULT4"] = "INFO"
        expected_loaded_config = [{'default': 'INFO', 'name': 'PARAM_WITH_DEFAULT1', 'value': 'DEBUG'},  {"name": "PARAM_WITHOUT_DEFAULT4", "value": "INFO"}]
        cm = ConfigManager(config_params)
        assert cm.loaded_config == expected_loaded_config
# ----------------------------------------------------------------------------------------------------------------------
